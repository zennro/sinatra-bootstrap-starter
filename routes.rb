require "./routes/user.rb"

get '/' do
  if logged_in?
    haml :index
  else
    redirect '/login'
  end
end
