DataMapper.setup(:default, ENV['DATABASE_URL'] || "sqlite3://#{Dir.pwd}/development.db")

class User
  include DataMapper::Resource
  
  attr_accessor :email, :password, :password_confirmation

  property :id,           Serial
  property :username,     String
  property :firstname,    String
  property :lastname,     String
  property :city,         String
  property :country,      String
  property :email,        String, :required => true, :unique => true, :format => :email_address
  property :password_hash,Text
  property :password_salt,Text
  property :token,        String
  property :created_at,   DateTime
  property :updated_at,   DateTime
  property :admin,        Boolean, :default => false
  
  
  validates_presence_of         :email
  validates_format_of :email, :as => :email_address
  validates_presence_of         :password
  validates_confirmation_of     :password
  validates_length_of           :password, :min => 6 

  after :create do
    self.token = SecureRandom.hex
  end

  def generate_token
    self.update!(:token => SecureRandom.hex)
  end

  def admin?
    self.admin
  end

end

DataMapper.finalize
DataMapper.auto_upgrade!